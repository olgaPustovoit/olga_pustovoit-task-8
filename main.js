const {City} = require('./City')
const {Capital} = require('./Capital')
const {Country} = require('./Country')

let city1 = new City('Kharkiv')
let city2 = new City('Lviv')

city1.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city2.setWeather().then(() => {console.log(city2)}).catch((error) => {console.log(error)})

let capital = new Capital('Kyiv')
capital.setAirport('Boryspil')
capital.setWeather().then(() => {console.log(capital)}).catch((error) => {console.log(error)})

const ukraine = new Country('Ukraine');

const cities = ['Kyiv', 'Odessa', 'Lviv', 'Kharkiv', 'Donetsk', 'Zhytomyr', 'Uman', 'Rivne', 'Vinnytsa', 'Poltava'];

cities.forEach(city => {
    ukraine.addCity(city);
});

ukraine.cities.forEach(city => {
    city.setWeather().then(() => {console.log(city)}).catch((error) => {console.log(error)});
    city.setForecast().then(() => {console.log(city)}).catch((error) => {console.log(error)});
});

ukraine.cities.sort((a, b) => {
    a.weather.temperature - b.weather.temperature
});

console.log(ukraine);