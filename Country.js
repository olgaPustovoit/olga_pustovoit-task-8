const {City} = require('./City')

class Country {
    constructor(name) {
        this.name = name;
    }

    cities = [];

    addCity(city) {
        const newCity = new City(city);
        this.cities.push(newCity);
    }

    removeCity(cityForRemove) {
        const indexForRemove = this.cities.findIndex(city => { city === cityForRemove });
        this.cities.splice(indexForRemove, 1);
    }

}

module.exports = { Country: Country }